#!/bin/bash

# Copyright 2019 Wojciech Kosior

# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# For more information, please refer to <http://unlicense.org/>

if [ -z "$1" ]; then echo "please pass number of images"; exit; fi

COMMAND="cafrun -np $1 ./integrator"

QUADRATURE_TYPES="newton-cotes gauss"

FUNCTIONS="exp sin poly"

# for simplicity always use the same
IBEG="-0.5"
IEND="1"

SUBINTERVALS="1 4 16 64 128"

VALID_POLY_ORDERS="0 1 2"

echo "\
# results of numerical integration of three different functions
# (exp, sin and some 10 degree polynomial) on [$IBEG; $IEND] interval
#
#FUNC ___QUAD_TYPE POLY_ORDER SUBINTERVALS _______COMPUTED_VALUE __________VALUE_ERROR"


for FUNC in $FUNCTIONS; do
    echo -n "# function $FUNC;  exact integral:"
    $COMMAND analytical $FUNC $IBEG $IEND 
    
    for TYPE in $QUADRATURE_TYPES; do
	for N in $SUBINTERVALS; do
	    for P in $VALID_POLY_ORDERS; do
		printf "%5s %12s %10s %12s " $FUNC $TYPE $P $N
		$COMMAND $TYPE $FUNC $IBEG $IEND $P $N
	    done
	done
    done
done
