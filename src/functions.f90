! Copyright 2019 Wojciech Kosior
  
! This is free and unencumbered software released into the public domain.

! Anyone is free to copy, modify, publish, use, compile, sell, or
! distribute this software, either in source code form or as a compiled
! binary, for any purpose, commercial or non-commercial, and by any
! means.

! In jurisdictions that recognize copyright laws, the author or authors
! of this software dedicate any and all copyright interest in the
! software to the public domain. We make this dedication for the benefit
! of the public at large and to the detriment of our heirs and
! successors. We intend this dedication to be an overt act of
! relinquishment in perpetuity of all present and future rights to this
! software under copyright law.

! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
! IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
! OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
! ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
! OTHER DEALINGS IN THE SOFTWARE.

! For more information, please refer to <http://unlicense.org/>

MODULE functions

  real(kind=8), private, parameter :: poly_coeffs(11) =            &
       [-11.0, 15.6, -4.6, 22.5, 8.1, 5.1, -0.3, -8.0, 0.0, -9.9, 2.2]

  INTERFACE
     FUNCTION analytical_integral(ibeg, iend) result(y)
       real(kind=8), intent(in) :: ibeg, iend
       real(kind=8) :: y       
     END FUNCTION analytical_integral
  END INTERFACE
  
CONTAINS

  FUNCTION my_exp(x) result(y)
    real(kind=8), intent(in) :: x
    real(kind=8) :: y
    y = exp(x)
  END FUNCTION my_exp

  FUNCTION my_sin(x) result(y)
    real(kind=8), intent(in) :: x
    real(kind=8) :: y
    y = sin(x)
  END FUNCTION my_sin

  FUNCTION my_poly(x) result(y)
    real(kind=8), intent(in) :: x
    real(kind=8) :: y
    integer(kind=4) :: i
    y = sum(poly_coeffs(:) * [1.0_8, (x ** [(i, i = 1, 10)])])
  END FUNCTION my_poly
  
  FUNCTION my_exp_int(ibeg, iend) result(y)
    real(kind=8), intent(in) :: ibeg, iend
    real(kind=8) :: y
    y = exp(iend) - exp(ibeg)
  END FUNCTION my_exp_int
  
  FUNCTION my_sin_int(ibeg, iend) result(y)
    real(kind=8), intent(in) :: ibeg, iend
    real(kind=8) :: y
    y = -cos(iend) + cos(ibeg)
  END FUNCTION my_sin_int

  FUNCTION my_poly_int_indefinite(x) result(y)
    real(kind=8), intent(in) :: x
    real(kind=8) :: y
    integer(kind=4) :: i, j
    y = sum(poly_coeffs(:) * (1 / real([(j, j = 1, 11)])) *        &
         (x ** [(i, i = 1, 11)]))
  END FUNCTION my_poly_int_indefinite
  
  FUNCTION my_poly_int(ibeg, iend) result(y)
    real(kind=8), intent(in) :: ibeg, iend
    real(kind=8) :: y
    y = my_poly_int_indefinite(iend) - my_poly_int_indefinite(ibeg)
  END FUNCTION my_poly_int
  
END MODULE functions
