# Copyright 2019 Wojciech Kosior

# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# For more information, please refer to <http://unlicense.org/>

FC = gfortran
FFLAGS = -std=f2008 -Wall -pedantic -fbounds-check -fimplicit-none \
	 -ffree-form -O2 -c

all : integrator integrator_single

main.o main.mod : quadratures.mod functions.mod

%.mod %.o : src/%.f90
	$(FC) $(FFLAGS) -fcoarray=lib $<

%.mod %_single.o : src/%.f90
	$(FC) $(FFLAGS) -fcoarray=single -o $*_single.o $<

integrator : main.o quadratures.o functions.o
	$(FC) -lcaf_mpi $^ -o $@

integrator_single : main_single.o quadratures_single.o functions_single.o
	$(FC) $^ -o $@

results: res/1image_results res/5images_results res/times

res/1image_results : integrator run.sh
	./run.sh 1 > $@

res/5images_results : integrator run.sh
	./run.sh 5 > $@

res/times : integrator integrator_single measure_times.sh
	./measure_times.sh > $@

clean :
	-rm integrator{,_single} *.{mod,o}

.PHONY : all clean results
