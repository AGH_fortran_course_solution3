#!/bin/bash

# Copyright 2019 Wojciech Kosior

# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# For more information, please refer to <http://unlicense.org/>

NIMAGES="1 2 3 5 8"
NSUBINTERVALS="200 20000 2000000 20000000 200000000"

echo "#SUBINTERVALS IMAGES __________TIME"


export TIMEFORMAT='%E'

for NSU in $NSUBINTERVALS; do
    for NIM in $NIMAGES; do
	printf "%13s %6s " $NSU $NIM
	
	# bash is weird... don't ask me...

	TIME=`bash -c "time cafrun -np $NIM ./integrator  \
	      	   gauss exp -1 1 1 $NSU" 2>&1 > /dev/null`

	if [ $NIM = 1 ]; then
    	    TIME=$TIME\(`bash -c "time ./integrator_single    \
	    	       gauss exp -1 1 1 $NSU" 2>&1 > /dev/null`\)
	fi

	printf "%14s\n" $TIME
    done
done
